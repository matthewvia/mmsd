/*
 *
 *  Visual Voicemail Daemon
 *
 *  Copyright (C) 2021, Chris Talbot <chris@talbothome.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 2 as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/mman.h>

#include <glib.h>
#include <glib/gprintf.h>

#include "phone-utils.h"

static void test_number_decode(gconstpointer data)
{
	char *output;

	const char phone_test1[] = "12065550110";
	const char phone_test2[] = "2065550110";
	const char phone_test3[] = "(206) 555-0110";
	const char phone_test4[] = "206-555-0110";

	const char phone_output[] = "+12065550110";

	const char email_test1[] = "testemail@example.com";

	output = phone_utils_format_number_e164(phone_test1, "US", TRUE);
	if (g_strcmp0(output, phone_output) != 0) {
		g_debug("Output %s is wrong! Expected output: %s\n", output, phone_output);
	}
	g_assert(g_strcmp0(output, phone_output) == 0);
	g_free(output);
	output = NULL;

	output = phone_utils_format_number_e164(phone_test2, "US", TRUE);
	if (g_strcmp0(output, phone_output) != 0) {
		g_debug("Output %s is wrong! Expected output: %s\n", output, phone_output);
	}
	g_assert(g_strcmp0(output, phone_output) == 0);
	g_free(output);
	output = NULL;

	output = phone_utils_format_number_e164(phone_test3, "US", TRUE);
	if (g_strcmp0(output, phone_output) != 0) {
		g_debug("Output %s is wrong! Expected output: %s\n", output, phone_output);
	}
	g_assert(g_strcmp0(output, phone_output) == 0);
	g_free(output);
	output = NULL;

	output = phone_utils_format_number_e164(phone_test4, "US", TRUE);
	if (g_strcmp0(output, phone_output) != 0) {
		g_debug("Output %s is wrong! Expected output: %s\n", output, phone_output);
	}
	g_assert(g_strcmp0(output, phone_output) == 0);
	g_free(output);
	output = NULL;

	output = phone_utils_format_number_e164(email_test1, "US", TRUE);
	if (g_strcmp0(output, output) != 0) {
		g_debug("Output %s is wrong! Expected output: %s\n", email_test1, output);
	}
	g_assert(g_strcmp0(output, email_test1) == 0);
	g_free(output);
	output = NULL;

	output = phone_utils_format_number_e164(email_test1, "US", FALSE);
	if (output) {
		g_debug("Output %s is not NULL and should be!\n", output);
	}
	g_assert(output == NULL);
	g_free(output);
	output = NULL;
}

int main(int argc, char **argv)
{
	g_test_init(&argc, &argv, NULL);

	g_test_add_data_func("/vvmutil/Number Decode Test",
				NULL, test_number_decode);


	return g_test_run();
}
